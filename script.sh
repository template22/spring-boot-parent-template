#!/bin/bash

if [[ $# -ne 5 ]]; then
    echo "Usage ./script.sh {project_name} {maven_group_id} {maven_version} {ci_project} {ci_file}"
    exit 2
fi

project_name=$1
maven_group_id=$2
maven_version=$3
ci_project=$4
ci_file=$5

echo $maven_group_id
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s/#MAVEN_GROUP_ID/$maven_group_id/g" {} +
echo $maven_version
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s/#MAVEN_VERSION/$maven_version/g" {} +
echo $ci_project
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_PROJECT~$ci_project~g" {} +
echo $ci_file
find . -type f -not -path "./.git/*" -not -path "./script.sh" -exec sed -i "s~#CI_FILE~$ci_file~g" {} +

#CI_PROJECT = questionapp1
#CI_FILE = .gitlab_ci_cd_java.yml
#MAVEN_GROUP_ID = com.gbournac.question
#MAVEN_VERSION = 0.0.1-SNAPSHOT
